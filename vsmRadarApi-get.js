var AWS = require("aws-sdk");
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    var params = {
        TableName: 'vsmRadarTable',
        KeyConditionExpression: 'deviceId = :id and #dt between :from and :to',
        ExpressionAttributeNames: {
            '#dt': 'datetime'
        },
        ExpressionAttributeValues: {
            ':id': parseFloat(event["queryStringParameters"].deviceId),
            ':from': parseFloat(event["queryStringParameters"].from),
            ':to': parseFloat(event["queryStringParameters"].to)
        }
    };

    docClient.query(params, function(err, data) {
        if (err) {
            console.log(err);
            var response = {
                statusCode: 500,
                body: JSON.stringify(err)
            };
            callback(response);
        }
        else{
            console.log(data);
            var response = {
                statusCode: 200,
                body: JSON.stringify(data["Items"])
            };
            callback(null, response);
        }
        
    });


};

var AWS = require("aws-sdk");
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    var params = {
        TableName: "vsmRadarTable",
        Item: event
    };

    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add vsm data", context.awsRequestId, ". Error JSON:", JSON.stringify(err, null, 2));
            var errorObj = {
                errorType: "InternalServerError",
                httpStatus: 500,
                requestId: context.awsRequestId,
                body: "ERROR JSON:" + JSON.stringify(err, null, 2)
            };
            callback(errorObj);
        }
        else {
            console.log("PutItem succeeded:", event.datetime);
            var msg = {
                httpStatus: 200,
                body: "Succeeded"
            };
            callback(null, msg);
        }
    });
};

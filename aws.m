awsurl='';
csvfile='./testdata.csv';
readopt=weboptions('MediaType', 'application/json');
writeopt=weboptions('ContentType', 'json');
csv_data=readtable(csvfile);
rorw='r';
if(rorw == 'w')
    for i = 1:100
        item = csv_data(i,[1 2 4:9]);
        s = struct('deviceId', item.device, 'datetime', item.datetime, ...
            'data', struct(...
            'ch0', [item.i0 item.q0], ...
            'ch1', [item.i1 item.q1], ...
            'ch2', [item.i2 item.q2] ...
            ));
        response=webwrite([awsurl '/add'], s,  readopt);
        if (response.httpStatus ~= 200)
            error('Http status error: %d (%s)', response.httpStatus, response.body);
        end
        fprintf("Put item %d\n", i);
        pause(0.2);
    end
else
    response = webread([awsurl '/get'], 'from', 1, 'to', 100, 'deviceId', 1, writeopt);
    response = struct2table(response);
    x = response.datetime;
    data = struct2table(response.data);
    y = cell2mat(data.ch0')';
    plot(x, y);
end



